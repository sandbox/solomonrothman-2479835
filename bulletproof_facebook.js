(function ($) {


  Drupal.behaviors.bulletproof_facebook = {
      attach: function (context, settings){
          $('body').once(function(){
              Drupal.behaviors.bulletproof_facebook.init(context);
              });

          $('.bpfb-connect-dl').click(function(){
              //console.log('Facebook authentication is clicked');
              $('#user-login #form_content').fadeOut();
              $('#register_form .replace_box').fadeOut();
              $('.facebook_connecting').fadeIn();

              FB.login(function(response) {
                  Drupal.behaviors.bulletproof_facebook.loginStatusChangeCallback(response);
                  // handle the response
              }, {scope: 'public_profile,email'});
             // Drupal.behaviors.bulletproof_facebook.checkLoginState();
          });

      },

      init: function(context){
       //   console.log('bulletproof_facebook is init');
       //   console.log(context);

          var jssource = '';
          if(Drupal.settings.bulletproof_facebook.mode == "dev"){
              var jssource = "//connect.facebook.net/en_US/sdk/debug.js";
          }
          else{
              jssource = "//connect.facebook.net/en_US/sdk.js";
          }

          window.fbAsyncInit = function() {
              FB.init({
                  appId      : Drupal.settings.bulletproof_facebook.appid,
                  cookie     : true,
                  xfbml      : true,
                  version    : 'v2.1',
                  status     : 'true'
              });
          };

          (function(d, s, id){
              var js, fjs = d.getElementsByTagName(s)[0];
              if (d.getElementById(id)) {return;}
              js = d.createElement(s); js.id = id;
              js.src = jssource;
              fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));



          var cookie_value = jQuery.cookie("DRUPAL_UID");
          if(cookie_value != 0)
          {
              console.log('authenticated');
              // Drupal user is authenticated...do some code...
          }
          else {
              // Drupal user anonymous user...do some code...
              console.log('anonymous');



          }
      },

      connectExisting: function(){

      },

      login: function(){

          console.log('access token is'+FB.getAuthResponse().accessToken);
          document.cookie = 'fb_token=' + FB.getAuthResponse().accessToken;
          this.PropagateSS(FB.getAuthResponse().accessToken);
      },

      logout: function(){
          document.cookie = 'fb_token=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
          this.PropagateSS();
      },


      PropagateSS: function(accesstoken){

        console.log('PropagateSS called');

          $.ajax({
              url: '/ajax/bulletproof-facebook/connect/' + FB.getAuthResponse().accessToken,
              dataType: 'json',
              success: function (data) {
                  console.log(data);
                  window.location.reload();
                  // do whatever you want.
                  // data.status: bool, true => login
                  // [data.message: string]
                  // [data.more: string]
              }
          });
      },

      loginStatusChangeCallback: function(response){
          console.log('loginStatusChangeCallback');
          console.log(response);

          if (response.status === 'connected') {
              //login();
              //check if already logged in as authenticated, if not login
            //  console.log('connected');
              Drupal.behaviors.bulletproof_facebook.login();


          } else {
              //   logout();
          //    console.log('disconnected');
              Drupal.behaviors.bulletproof_facebook.logout();

          }
      },

      checkLoginState: function(){
          //console.log('checkLoginStage was called');

          FB.getLoginStatus(function(response){
              Drupal.behaviors.bulletproof_facebook.loginStatusChangeCallback(response);
          });

  //        setTimeout("FB.getLoginStatus(function(response){console.log('got login status'); console.log(response); Drupal.behaviors.bulletproof_facebook.loginStatusChangeCallback(response);})", 4000);



      },



  };

//  console.log('bulletproof_facebook.js is ready');



}(jQuery));